module key_led(
input sys_clk , //50Mhz????
input sys_rst_n, //????????
input [3:0] key, //??????
output reg [3:0] led //LED????
);

//reg define
reg [23:0] cnt;
reg [1:0] led_control;

//????0.2s????
always @ (posedge sys_clk or negedge sys_rst_n) begin
    if(!sys_rst_n)
        cnt <= 24'd0;
    else if(cnt < 24'd9_999_999) 
        //else if(cnt < 24'd9) //?????
        cnt<= cnt + 1;
    else
        cnt<= 0;
end

//??led??????
always @(posedge sys_clk or negedge sys_rst_n) begin
    if (!sys_rst_n)
        led_control <= 2'b00;
    else if(cnt == 24'd9_999_999)
        //else if(cnt == 24'd9) //?????
        led_control <= led_control + 1'b1;
    else
        led_control <= led_control;
end

//???????????
always @(posedge sys_clk or negedge sys_rst_n) begin
    if(!sys_rst_n) begin
        led <= 4'b0000;
    end
    else if(key[0]== 0) 
        //??1?????????????? 
        case (led_control) 
            2'b00 : led <= 4'b1000; 
            2'b01 : led <= 4'b0100; 
            2'b10 : led <= 4'b0010; 
            2'b11 : led <= 4'b0001; 
            default : led <= 4'b0000; 
        endcase
    else if (key[1]==0)
        //??2?????????????? 
        case (led_control) 
            2'b00 : led <= 4'b0001; 
            2'b01 : led <= 4'b0010; 
            2'b10 : led <= 4'b0100; 
            2'b11 : led <= 4'b1000; 
            default : led <= 4'b0000; 
            endcase 
    else if (key[2]==0) 
        //??3????LED?? 
        case (led_control) 
            2'b00 : led <= 4'b1111; 
            2'b01 : led <= 4'b0000; 
            2'b10 : led <= 4'b1111; 
            2'b11 : led <= 4'b0000; 
            default : led <= 4'b0000; 
        endcase 
    else if (key[3]==0) 
        //??4????LED?? 
        led = 4'b1111; 
    else 
        led <= 4'b0000; 
        //???????LED?? 67
    end
endmodule












